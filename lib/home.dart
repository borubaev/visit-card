import 'package:flutter/material.dart';
import 'package:input_textformfield/secondpage/secondpage.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String? phoneNumber;
  String? email;
  bool active = false;

  void isActive() {
    if (phoneNumber != null && email != null) {
      if (phoneNumber!.isEmpty || email!.isEmpty) {
        active = false;
      } else {
        active = true;
      }
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff056C5C),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Center(
          child: Text(
            'Тапшырма 4',
            style: TextStyle(color: Colors.black),
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Shaiyr Borubaev',
              style: TextStyle(
                  fontSize: 58, color: Colors.white, fontFamily: 'Sofia'),
            ),
            const Text(
              'Flutter developer',
              style: TextStyle(
                  fontSize: 32,
                  color: Colors.white,
                  fontWeight: FontWeight.w100,
                  decoration: TextDecoration.underline),
            ),
            const SizedBox(
              height: 15,
            ),
            Container(
              color: Colors.white,
              width: double.infinity,
              height: 48,
              child: TextFormField(
                onChanged: (value) {
                  phoneNumber = value;
                  isActive();
                },
                style: const TextStyle(
                    fontSize: 20,
                    color: Color(0xff056C5C),
                    fontWeight: FontWeight.w600),
                decoration: const InputDecoration(
                  prefixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(40, 10, 20, 10),
                    child: Icon(
                      Icons.phone_outlined,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              color: Colors.white,
              width: double.infinity,
              height: 48,
              child: TextFormField(
                onChanged: (value) {
                  email = value;
                  isActive();
                },
                style: const TextStyle(
                    fontSize: 20,
                    color: Color(0xff056C5C),
                    fontWeight: FontWeight.w600),
                decoration: const InputDecoration(
                  prefixIcon: Padding(
                    padding: EdgeInsets.fromLTRB(40, 10, 20, 10),
                    child: Icon(
                      Icons.mail_outlined,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: active
                  ? () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const SecondPage()));
                    }
                  : null,
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.fromLTRB(50, 10, 50, 10)),
              child: const Text('Start'),
            ),
          ],
        ),
      ),
    );
  }
}
