import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  const SecondPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff056C5C),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text('Page for Rich Mens'),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
              'Im Rich',
              style: TextStyle(
                  fontFamily: 'Sofia',
                  fontWeight: FontWeight.w600,
                  fontSize: 70,
                  color: Colors.black),
            ),
            Image.asset(
              "assets/images/almaz.png",
              width: 350,
              height: 350,
            )
          ],
        ),
      ),
      floatingActionButton: const Icon(
        Icons.arrow_back,
        color: Colors.white,
      ),
    );
  }
}
